System.config({
  baseURL: "/Scripts/MWA",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "optimisation.modules.system"
    ]
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*",
    "google-maps": "https://maps.googleapis.com/maps/api/js?key=AIzaSyCIEp7Gy0XjzBRxDZ4dlqcLx24B3vnxiyY&"
  },
  bundles: {
    "main-bundle.js": [
      "main.js",
      "utils/clickUtils.js",
      "components/secondaryNav.js",
      "github:DEGJS/domUtils@1.0.0.js",
      "github:DEGJS/domUtils@1.0.0/domUtils.js",
      "github:DEGJS/moduleLoader@master.js",
      "github:DEGJS/moduleLoader@master/moduleLoader.js"
    ],
    "components/videoOverlay-bundle.js": [
      "components/videoOverlay.js"
    ],
    "components/interests-bundle.js": [
      "components/interests.js",
      "plugins/tabbedList.js",
      "github:DEGJS/domEvent@1.0.0.js",
      "github:DEGJS/domEvent@1.0.0/domEvent.js",
      "github:DEGJS/objectUtils@1.0.0.js",
      "github:DEGJS/objectUtils@1.0.0/objectUtils.js"
    ],
    "components/insights-bundle.js": [
      "components/insights.js",
      "utils/scrollUtils.js",
      "github:DEGJS/fetchUtils@master.js",
      "github:DEGJS/fetchUtils@master/fetchUtils.js"
    ],
    "components/locationsList-bundle.js": [
      "components/locationsList.js",
      "github:DEGJS/fetchUtils@master.js",
      "github:DEGJS/fetchUtils@master/fetchUtils.js",
      "github:DEGJS/eventAggregator@1.0.0.js",
      "github:DEGJS/eventAggregator@1.0.0/eventAggregator.js",
      "github:DEGJS/breakpoints@2.0.0.js",
      "github:DEGJS/breakpoints@2.0.0/breakpoints.js",
      "utils/svgDomUtils.js"
    ],
    "components/advisorSearch-bundle.js": [
      "components/advisorSearch.js",
      "utils/scrollUtils.js",
      "github:DEGJS/fetchUtils@master.js",
      "github:DEGJS/fetchUtils@master/fetchUtils.js",
      "github:HeinrichFilter/systemjs-plugin-googlemaps@master.js",
      "github:HeinrichFilter/systemjs-plugin-googlemaps@master/googlemaps.js"
    ],
    "components/newsroomList-bundle.js": [
      "components/newsroomList.js",
      "utils/scrollUtils.js",
      "github:DEGJS/fetchUtils@master.js",
      "github:DEGJS/fetchUtils@master/fetchUtils.js"
    ],
    "components/customValidation-bundle.js": [
      "components/customValidation.js"
    ]
  },

  meta: {
    "google-maps": {
      "build": false,
      "loader": "systemjs-googlemaps"
    },
    "components/advisorSearch.js": {
      "deps": [
        "systemjs-googlemaps"
      ]
    }
  },

  map: {
    "DEGJS/breakpoints": "github:DEGJS/breakpoints@2.0.0",
    "DEGJS/domEvent": "github:DEGJS/domEvent@1.0.0",
    "DEGJS/domUtils": "github:DEGJS/domUtils@1.0.0",
    "DEGJS/eventAggregator": "github:DEGJS/eventAggregator@1.0.0",
    "DEGJS/fetchUtils": "github:DEGJS/fetchUtils@master",
    "DEGJS/moduleLoader": "github:DEGJS/moduleLoader@master",
    "DEGJS/objectUtils": "github:DEGJS/objectUtils@1.0.0",
    "babel": "npm:babel-core@5.8.34",
    "babel-runtime": "npm:babel-runtime@5.8.34",
    "core-js": "npm:core-js@1.2.6",
    "systemjs-googlemaps": "github:HeinrichFilter/systemjs-plugin-googlemaps@master",
    "github:DEGJS/breakpoints@2.0.0": {
      "eventAggregator": "github:DEGJS/eventAggregator@1.0.0"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.34": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@1.2.6": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});

var testDefs = {
	fetch: self.fetch,
	assign: Object.assign
}

var bundles = [{filename: 'main-bundle.js',tests:['fetch','assign']}],
map = {};

function mapPolyfilledBundles() {
	for(var i = 0; i < bundles.length; i++) {
		mapPolyfilledBundle(bundles[i]);	
	}
	System.config({ map: map });
}

function mapPolyfilledBundle(bundle) {
	var polyfilledBundleFilename = buildPolyfilledBundleFilename(bundle);
	map[bundle.filename] = polyfilledBundleFilename;
}

function buildPolyfilledBundleFilename(bundle) {
	var filename = bundle.filename.replace(/\.js$/, '');			

	for(var i = 0; i < bundle.tests.length; i++) {
		var testName = bundle.tests[i];
		var testDef = testDefs[testName];
		if(testDef)
			continue;
		
		filename += '-' + testName;
	}

	return filename + '.js'
}

mapPolyfilledBundles();